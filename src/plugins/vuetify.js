import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

const opts = {
    theme: {
        themes: {
            light: {
                primary: '#004682',
                secondary: '#f0f3f5'
            }
        }
    }
}

export default new Vuetify(opts)