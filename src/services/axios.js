import axios from 'axios';

export const HTTP = axios.create({
  baseURL: window.location.hostname == 'localhost' ? 'http://localhost:8000' : 'https://' + window.location.hostname + '/api',
  headers: {
    Authorization: 'Bearer {token}'
  }
})