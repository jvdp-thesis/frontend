import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
// import { HTTP } from '@/services/axios.js'

export default new Vuex.Store({
  state: {
    incidents: [],
    alerts: [],
    api_info: {},
    dataLoading: true,
    bins: [],
    detectionSystemCount: 0
  },
  mutations: {
    addIncident (state, incident) {
      state.incidents.push(incident)
    },
    setIncidents (state, data) {
      state.incidents = data
      state.dataLoading = false
    },
    updateIncident (state, data) {
      let i = state.incidents.findIndex(x => x.id === data.id)
      state.incidents[i] = Object.assign(state.incidents[i], data.data)
    },
    addRelation (state, data) {
      let i = state.incidents.findIndex(x => x.id === data.incident_id)
      if(data.tweet) state.incidents[i].related_tweets.push(data.tweet)
      state.incidents[i].related_tweet_ids.push(data.tweet_id)
    },
    addAlert (state, alert) {
      state.alerts.push(alert)
    },
    deleteAlert (state, index) {
      state.alerts.splice(index, 1)
    },
    setApiInfo (state, data) {
      state.api_info = data
    },
    setDetectionSystemCount (state, data) {
      state.detectionSystemCount = data
    },
    markIncidentAsSeen (state, id) {
      let i = state.incidents.findIndex(x => x.id === id)
      state.incidents[i]["seen"] = true
    }
  },
  actions: {
    addIncident (context, incident) {
      if(!this.state.incidents.find(x => x.id === incident.id)) {
        incident["seen"] = false
        context.commit('addIncident', incident)
      }else{
        // Duplicate
      }
    },
    setIncidents (context, data) {
      context.commit('setIncidents', data)
    },
    updateIncident (context, data) {
      context.commit('updateIncident', data)
    },
    markIncidentAsSeen (context, id) {
      context.commit('markIncidentAsSeen', id)
    },
    addRelation (context, data) {
      context.commit('addRelation', data)
    },
    addAlert (context, alert) {
      if(!this.state.alerts.find(x => x.id === alert.id)) context.commit('addAlert', alert)
    },
    deleteAlert (context, id) {
      let index = this.state.alerts.findIndex(x => x.id === id)
      if(index > -1) context.commit('deleteAlert', index)
    },
    setApiInfo (context, data) {
      context.commit('setApiInfo', data)
    },
    setDetectionSystemCount (context, data) {
      context.commit('setDetectionSystemCount', data)
    }
  },
  getters: {
    getIncidents: state => {
      return state.incidents
    },
    getIncident: state => (id) => {
      return state.incidents.find(x => x.id === id)
    },
    getIncidentIDs: state => {
      let ids = []
      for(let i = 0; i < state.incidents.length; i++) {
        ids.push(state.incidents[i].id)
      }
      return ids
    },
    getLastIncidentID: state => {
      return state.incidents[state.incidents.length - 1]?.id
    },
    getAlerts: state => {
      return state.alerts
    },
    getApiInfo: state => {
      return state.api_info
    },
    getLoadingState: state => {
      return state.dataLoading
    },
    getSystemCount: state => {
      return state.detectionSystemCount
    }
  },
  modules: {
  }
})
